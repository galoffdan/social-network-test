package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        String postsSourcePath =  "src/itis/socialtest/resources/PostDatabase.csv";
        String authorsSourcePath = "src/itis/socialtest/resources/Authors.csv";
        new MainClass().run(postsSourcePath, authorsSourcePath);
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        parseFile(postsSourcePath, authorsSourcePath);
        allPosts.forEach(System.out::println);
        System.out.println("Посты, опубликованные сегодня: ");
        analyticsService.findPostsByDate(allPosts, "17.04.2021").forEach(System.out::println);

        analyticsService.findAllPostsByAuthorNickname(allPosts, "varlamov");
        analyticsService.checkPostsThatContainsSearchString(allPosts, "Россия");
        System.out.println("Самый популярный автор" + analyticsService.findMostPopularAuthorNickname(allPosts));
    }
    private List<String> splitStr(String str){
        return Arrays.stream(str.split(",")).map(String::trim).collect(Collectors.toList());
    }
    private void parseFile(String postsSourcePath, String authorsSourcePath){
        allPosts = new ArrayList<>();
        List<Author> authors = new ArrayList<>();
        BufferedReader bf;
        try {
            bf = new BufferedReader(new FileReader(authorsSourcePath));
            String str;
            while((str = bf.readLine()) != null){
                List<String> data = splitStr(str);
                authors.add(new Author(Long.parseLong(data.get(0)), data.get(1), data.get(2)));
            }
            bf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bf = new BufferedReader(new FileReader(postsSourcePath));
            String str;
            while ((str = bf.readLine()) != null){
                List<String> data = splitStr(str);
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 3; i < data.size(); i++) {
                    stringBuilder.append(data.get(i));
                }
                data.set(3, stringBuilder.toString());
                allPosts.add(new Post(data.get(2).replace('T', ' '),
                        data.get(3),
                        Long.parseLong(data.get(1)),
                        authors.stream()
                                .filter(author -> author.getId() == Long.parseLong(data.get(0))).findFirst().get()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
